package section1_intro.part1_language_basics;


public class Point {
    int x;
    int y;

    double euclideanDistanceTo(Point otherPoint) {
        //calculate distance - can you implement this?
        double firstPoint = (otherPoint.y - y);
        double secondPoint = (otherPoint.x - x);
        return Math.hypot(firstPoint, secondPoint);
    }
}
