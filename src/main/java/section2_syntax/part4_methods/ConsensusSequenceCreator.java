/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package section2_syntax.part4_methods;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class ConsensusSequenceCreator {
    private String[] sequences;
    private boolean iupac;
    private String[] ambiguities;
    private String consensus = "";

    /**
     * testing main.
     * @param args 
     */
    public static void main(String[] args) {
        String[] sequences = new String[4];
        sequences[0] = "GAAT";
        sequences[1] = "GAAA";
        sequences[2] = "GATT";
        sequences[3] = "GAAC";
        
        ConsensusSequenceCreator csc = new ConsensusSequenceCreator();
        String consensus;
        consensus = csc.createConsensus(sequences, true);
        System.out.println("consensus = " + consensus);
        //consensus should equal "GAWH"
        consensus = csc.createConsensus(sequences, false);
        //consensus should equal "GA[A/T][A/T/C]"
        System.out.println("consensus = " + consensus);
    }

    /**
     * creates a consensus sequence from the given array of sequences.
     * @param sequences the sequences to scan for consensus
     * @param iupac flag to indicate IUPAC (true) or bracket notation (false)
     * @return consensus the consensus sequence
     */
    public String createConsensus(String[] sequences, boolean iupac) {
        this.sequences = sequences;
        this.iupac = iupac;
        this.ambiguities = new String[sequences[0].length()];
        ArrayList allPossibleCharacters = new ArrayList<>();
        System.out.println(allPossibleCharacters);
        buildConsensus(buildAmbiguities());
        return this.consensus;
    }

    private void buildConsensus(ArrayList arrayList) {
        //Map<ArrayList, Integer> IUPAC = new Hashmap<>;
        if (this.iupac) {
            for (int position = 0; position < arrayList.size(); position++) {

            }
            this.consensus = "empty";
        } else {
            for (int position = 0; position < arrayList.size(); position++) {

            }
            this.consensus = "empty";
        }

    }

    private ArrayList buildAmbiguities() {
        ArrayList allPossibleCharacters = new ArrayList<>();
        for (int position = 0; position < sequences[0].length(); position++) {
            Set<Character> allCharacters = new HashSet<>();
            for (int sequenceIndex = 0; sequenceIndex < sequences.length; sequenceIndex++) {
                allCharacters.add(sequences[sequenceIndex].charAt(position));
            }
            allPossibleCharacters.add(allCharacters);
        }
        return allPossibleCharacters;
    }

}
