/*
 * Copyright (c) 2014 Michiel Noback
 * All rights reserved
 * www.bioinf.nl, www.cellingo.net
 */
package section2_syntax.part3_flow_control;

/**
 *
 * @author michiel
 */
public class AllSubstringsPrinter {
    /**
     * main method serves development purposes only.
     * @param args the args
     */
    public static void main(final String[] args) {
        AllSubstringsPrinter asp = new AllSubstringsPrinter();
        asp.printAllSubstrings("GATCG", true, true); //should print left truncated, left aligned
        asp.printAllSubstrings("GATCG", true, false);
        asp.printAllSubstrings("GATCG", false, true);
        asp.printAllSubstrings("GATCG", false, false);
    }

    /**
     * Prints all possible substrings according to arguments.
     * @param stringToSubstring the string to substring
     * @param leftTruncated flag to indicate whether the substrings should be truncated from the left (or the right)
     * @param leftAligned flag to indicate whether the substrings should be printed left-aligned (or right-aligned)
     */
    public void printAllSubstrings(String stringToSubstring, boolean leftTruncated, boolean leftAligned) {
        //your code
        int lengthString = stringToSubstring.length();
        for (int i = 0; i < lengthString; i++) {
            if (leftTruncated && leftAligned) {
                System.out.println(stringToSubstring.substring(i, lengthString));
            }

            else if (leftTruncated && !leftAligned) {
                for (int j = 0; j < i; j++) {
                    System.out.print(" ");
                }
                System.out.println(stringToSubstring.substring(i, lengthString));
            }

            else if (!leftTruncated && leftAligned) {
                System.out.println(stringToSubstring.substring(0, lengthString - i));
            }

            else  {
                for (int j = 0; j < i; j++) {
                    System.out.print(" ");
                }
                System.out.println(stringToSubstring.substring(0, lengthString - i));
            }
        }
    }
}
