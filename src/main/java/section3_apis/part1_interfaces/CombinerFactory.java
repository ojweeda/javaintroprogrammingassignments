package section3_apis.part1_interfaces;

public class CombinerFactory {

    static StringCombiner getQuotedCombiner() {
        /**
         * This method returns a StringCombiner that will surround the given arguments with double quotes,
         * separated by spaces and the result surrounded by single quotes.
         * For example, this call combiner.combine("one", "two") will return '"one" "two"'
         * @return quotedCombiner
         */
        return new StringCombiner() {
            @Override
            public String combine(String first, String second) {
                final String quotedCombined = String.format("\'\"%s\" \"%s\"\'", first, second);
                System.out.println(quotedCombined);
                return quotedCombined;
            }
        };
    }


    static StringCombiner getReversedCombiner() {
        /**
         * This method returns a StringCombiner that will return the given arguments combined reversed and original,
         * concatenated with a space in between.
         * For example, combiner.combine("one", "two") will return "oneeno twoowt" (the quotes are not included
         * in the return value).
         * @return reversedCombiner
         */
        return new StringCombiner() {
            @Override
            public String combine(String first, String second) {
                final String revFirst = new StringBuilder(first).reverse().toString();
                final String reSecond= new StringBuilder(second).reverse().toString();
                String reversed = String.format("%s%s %s%s", first, revFirst, second, reSecond);
                System.out.println(reversed);
                return reversed;
            }
        };
    }


    static StringCombiner getAsciiSumCombiner() {
        /**
         * <strong>Challenge!</strong>
         * This method returns a StringCombiner that will return the given arguments where the characters of both
         * arguments are converted to their ASCII values and then the summed. These numbers are combined with a space
         * in between and returned.
         *
         * For example, combiner.combine("one", "two") will return "322 346" (111 + 110 + 101 and 116 + 119 + 111).
         * @return reversedCombiner
         */
        return new StringCombiner() {
            @Override
            public String combine(String first, String second) {
                final int sumFirst = first.chars().sum();
                final int sumSecond = second.chars().sum();
                System.out.println(sumFirst + " " + sumSecond);
                return sumFirst + " " + sumSecond;
            }
        };
    }


}
