package section3_apis.part3_protein_sorting;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class ProteinWeightComparator implements Comparator<Protein> {
    @Override
    public int compare(Protein p1, Protein p2) {
        String[] p1Sequence = p1.getAminoAcidSequence().split("");
        String[] p2Sequence = p2.getAminoAcidSequence().split("");
        HashMap<String, Float> aminoAcidsWeights = new HashMap<>() {{
            put("I", 131.1736f);
            put("L", 131.1736f);
            put("K", 146.1882f);
            put("M", 149.2124f);
            put("F", 165.1900f);
            put("T", 119.1197f);
            put("W", 204.2262f);
            put("V", 117.1469f);
            put("R", 174.2017f);
            put("H", 155.1552f);
            put("A", 89.0935f);
            put("N", 132.1184f);
            put("D", 133.1032f);
            put("C", 121.1590f);
            put("E", 147.1299f);
            put("Q", 146.1451f);
            put("G", 75.0669f);
            put("P", 115.1310f);
            put("S", 105.0930f);
            put("Y", 181.1894f);
        }};
        float p1Weight = 0;
        float p2Weight = 0;
        for (String aminoAcid:p1Sequence) {
            p1Weight += aminoAcidsWeights.get(aminoAcid);
        }
        System.out.println(p1Weight);
        for (String aminoAcid:p2Sequence) {
            p2Weight += aminoAcidsWeights.get(aminoAcid);
        }
        System.out.println(p2Weight);

        return (int) p1Weight.compareTo((int) p2Weight);
        p1.getAccession().toLowerCase().compareTo(p2.getAccession().toLowerCase());
    }
}
