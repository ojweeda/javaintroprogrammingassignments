package section3_apis.part3_protein_sorting;

import java.util.Comparator;

public class AccessionNumberComparator implements Comparator<Protein> {

    @Override
    public int compare(Protein p1, Protein p2) {
        return p1.getAccession().toLowerCase().compareTo(p2.getAccession().toLowerCase());
    }
}
