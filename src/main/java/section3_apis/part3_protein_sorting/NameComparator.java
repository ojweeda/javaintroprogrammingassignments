package section3_apis.part3_protein_sorting;

import java.util.Comparator;

public class NameComparator implements Comparator<Protein> {
    @Override
    public int compare(Protein p1, Protein p2) {
        return p1.getName().compareTo(p2.getName());
    }
}
