package section3_apis.part3_protein_sorting;

import java.util.Comparator;

public class GOAnnotationComparator implements Comparator<Protein> {
    @Override
    public int compare(Protein p1, Protein p2) {
        /*
        first on biological process
        second on cellular component
        last on molecular function
         */
        int bioProcess = p1.getGoAnnotation().getBiologicalProcess().compareTo(p2.getGoAnnotation().getBiologicalProcess());
        if (bioProcess == 0) {
            int cellComp = p1.getGoAnnotation().getCellularComponent().compareTo(p2.getGoAnnotation().getCellularComponent());
            if (cellComp == 0) {
                int molFunc = p1.getGoAnnotation().getMolecularFunction().compareTo(p2.getGoAnnotation().getMolecularFunction());
                return molFunc;
            } else {
                return cellComp;
            }
        } else {
            return bioProcess;
        }
    }
}
