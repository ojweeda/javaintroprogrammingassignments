package section3_apis.part2_collections;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class StudentAdminDataReader {

    private Map<Integer, Student> students = new HashMap<>();
    private Map<Integer, Course> courses = new HashMap<>();

    private StudentAdmin studentAdmin;

    public StudentAdmin importAll(String studentsFile, String courseResultsFile) {
        this.studentAdmin = new StudentAdmin();
        readStudents(studentsFile);
        readCourseResults(courseResultsFile);
        return studentAdmin;
    }

    private void readStudents(String studentsFile) {
        Path filePath = initFile(studentsFile);
        readFile(filePath, new StudentLineHandler());
//        studentAdmin.printStudents();
    }

    private void readCourseResults(String courseResultsFile) {
        Path filePath = initFile(courseResultsFile);
        readFile(filePath, new CoursesLineHandler());
//        studentAdmin.printCourses();
    }

    private void readFile(Path filePath, LineHandler lineHandler) {
        int lineCount = 0;
        try (BufferedReader reader = Files.newBufferedReader(filePath)) {
            String line;
            while ((line = reader.readLine()) != null) {
                lineCount++;
                if (lineCount == 1) continue;
                //System.out.println("line = " + line);
                lineHandler.processLine(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Path initFile(String fileName) {
        Path path = Paths.get(fileName);
        File file = path.toFile();
        if (! (file.exists() && file.canRead())) {
            throw new IllegalArgumentException("file " + file + " does not exist or is not readable.");
        }
        return path;
    }

    private interface LineHandler {
        void processLine(String line);
    }

    /**
     * processes each line of the file students.txt
     */
    private class StudentLineHandler implements LineHandler {
        @Override
        public void processLine(String line) {
            String[] elements = line.split("\t");
            //System.out.println(Arrays.toString(elements));
            //-------------------  YOUR CODE HERE   -------------------//
            final int studentId = Integer.parseInt(elements[0]);
            final String firstName = elements[2];
            final String lastName = elements[1];
            Student student = new Student(studentId, firstName, lastName);
            studentAdmin.addStudent(student);
        }
    }

    /**
     * processes each line of the file courses.csv
     */
    private class CoursesLineHandler implements LineHandler {
        @Override
        public void processLine(String line) {
            String[] elements = line.split(";");
            //System.out.println(Arrays.toString(elements));
            //-------------------  YOUR CODE HERE   -------------------//
            final String courseId = elements[0];
            final int studentId = Integer.parseInt(elements[1]);
            final double grade = Double.parseDouble(elements[2]);
            Course course = new Course(courseId);
            studentAdmin.addCourse(course, studentId, grade);
        }
    }


    public static void main(String[] args) {
        StudentAdminDataReader dataReader = new StudentAdminDataReader();
        dataReader.importAll("data/students.txt", "data/courses.csv");
    }
}
