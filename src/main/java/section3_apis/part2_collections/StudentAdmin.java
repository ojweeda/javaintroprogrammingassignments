package section3_apis.part2_collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StudentAdmin {
    private Map<Integer, Student> students = new HashMap<>();
    private Map<String, Map<Integer, Double>> coursesGrades = new HashMap<>();

    public void addStudent(Student student) {
        students.put(student.getStudentId(), student);
    }

    public void addCourse(Course course, int studentId, double grade) {
        if (!coursesGrades.containsKey(course.getCourseId())) {
            Map<Integer, Double> studentIdGrade = new HashMap<>();
            studentIdGrade.put(studentId, grade);
            coursesGrades.put(course.getCourseId(), studentIdGrade);
        } else {
            coursesGrades.get(course.getCourseId()).put(studentId, grade);
        }
    }

    public void printStudents() {
        System.out.println(students);
    }

    public void printCourses() {
        System.out.println(coursesGrades);
    }

    /**
     * Returns the students that are present in the database.
     * If the substring is *, all students will be returned. Else,
     * a simple case insensitive substring match to both first name and family name will be performed.
     *
     * @return students
     */

    public List<Student> getStudents(String substring) {
        final String WILDCARD = "*";
        List<Student> students = new ArrayList<>();
        if (substring.equals(WILDCARD)) {
            for (int studentId : this.students.keySet()) {
                Student student = this.students.get(studentId);
                students.add(student);
            }
        } else {
            for (int studentId : this.students.keySet()) {
                Student student = this.students.get(studentId);
                if (student.getFirstName().toLowerCase().contains(substring.toLowerCase())) {
                    students.add(student);
                } else if (student.getLastName().toLowerCase().contains(substring.toLowerCase())) {
                    students.add(student);
                }
            }
        }
        System.out.println(students);
        return students;
    }

    /**
     * Returns the grade of a student for the given course
     *
     * @param student
     * @param course
     * @return grade
     */
    public double getGrade(Student student, Course course) {
        String askedCourse = course.getCourseId();
        int askedStudent = student.getStudentId();
        System.out.println(coursesGrades.get(askedCourse).get(askedStudent));
        return coursesGrades.get(askedCourse).get(askedStudent);
    }

    /**
     * returns all grades for a student, as [key=CourseID]:[value=Grade] Map
     *
     * @param student
     * @return grades
     */
    public Map<String, Double> getGradesForStudent(Student student) {
        Map<String, Double> studentGrades = new HashMap<>();
        for (String courseId : coursesGrades.keySet()) {
            if (coursesGrades.get(courseId).get(student.getStudentId()) != null) {
                studentGrades.put(courseId, coursesGrades.get(courseId).get(student.getStudentId()));
            }
        }
        System.out.println(studentGrades);
        return studentGrades;
    }

    /**
     * Returns all grades for a course, as [key=Student]:[value=Grade] Map
     *
     * @param course the course
     * @return grades
     */
    public Map<Student, Double> getGradesForCourse(Course course) {
        Map<Student, Double> courseGrades = new HashMap<>();
        for (int studentId : coursesGrades.get(course.getCourseId()).keySet()) {
            courseGrades.put(students.get(studentId), coursesGrades.get(course.getCourseId()).get(studentId));
        }
        System.out.println(courseGrades);
        return courseGrades;
    }
}
